# Tailwind UI Starter Brief

## Synopsis

Create a simple project starter layout with webpack and other npm packages to support css & js development in a server rendered html web application.

## Structure / Description

```
tailwind-ui-starter/
├── PROJECT-BRIEF.md            # This document
├── README.md                   # Instructions for using and enhancing this starter kit 
├── public                      # The location 'built' and static assets are served from
│   ├── css                     # Saas files that have been processed into css from ../ui-src/css
│   ├── fonts                   # Local copies of any font files used, such as the 'Inter' font family as recommended ny tailwind
│   ├── img                     # Images
│   ├── index.html              # Sample usage of the compiled assets in static html, and a responsive component
│   └── js                      # ES5 JavaScript files transpiled from typescript or ES6 JavaScript from ../ui-src/js
└── ui-src
    ├── css
    │   └── app.scss            # Sample sass file
    ├── js
    │   └── app.ts              # Sample typescript file
    └── package.json            # Packages from npm, watch/build scripts 

```

## Instructions

Following the [tailwind ui documentation](https://tailwindui.com/documentation), extend this repository to have the following capabilities:

1. Webpack to watch and build files from `ui-src` into `public` according to their type
1. Transpile typescript and javascript to ES5
1. Production build step that does all of the optimizations such as stripping out none used tailwind css
1. All tailwind and tailwind ui optional packages installed via npm (eg `tailwindcss@latest, @tailwindcss/forms, @tailwindcss/typography, @tailwindcss/aspect-ratio`) and Inter font
1. All dependencies installed via npm or stored locally to be served from `./public` no CDN's
1. A demo of at least one working responsive tailwind ui component in `./public/index.html` that uses alpine js such as [this component](https://tailwindui.com/components/application-ui/application-shells/stacked#component-10058606cac5398d7fa2c73b64089874)
1. Added font awesome npm package to package.json and an icon displayed on `./public/index.html`
1. Updated README with usage instructions, and guide to add more JS, SCSS to webpack and `./public/index.html` when additional npm packages are needed
1. NPM scripts inside `package.json` to watch (dev mode) and build (prod mode) outputs
1. Prod build should optimise the generated tailwind css so components not used are stripped from the bundle
1. A reasonable lighthouse score with no major errors or warnings
